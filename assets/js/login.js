//console.log("hello from login")
let loginForm = document.querySelector("#loginUser")

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value	
	let password = document.querySelector("#password").value

	// console.log(email)
	// console.log(password)	
	//lets create first a validation that will allow us to determine if the input fields are not empty
	if(email == '' || password == '') {
		alert("Please input email and/or password!")
	} else {
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			}, 
			body: JSON.stringify({
				email: email, 
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			//to check if the data has benn caught, lets display it first in the console.
			console.log(data)
			//upon successful authentication it will return a JWT
			//lets create a control structure to determine if the JWT has been generated together with the user credentials in a object format. 
			if(data.access){
				//we are going to store now the JWT inside our localStorage
				localStorage.setItem('token', data.access) 
				//once that the id is authentocated successfuly using the accesstoken then it should redirect the user to the user's profile page
				window.location.replace("./profile.html")
			} else {
				//create an else branch that will run if an access key is not found 
				alert("Something Went Wrong, Check your Credentials!")
			}
		})
	}
})